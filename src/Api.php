<?php

namespace Payone;

use Payone\Api\RequestBuilder\ServerApiRequestBuilder;
use Payone\Api\Server;
use Payone\Api\DataMapper;
use Payone\Api\ResponseParser;
use Payone\Api\Notification;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Api
{
    /**
     * @var array
     */
    private $options;
    /**
     * @var Notification
     */
    private $notification;
    /**
     * @var Server
     */
    private $client;

    public function __construct(array $options, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->options = $resolver->resolve($options);

        /** @var Serializer $serializer */
        $serializer = SerializerBuilder::create()->build();
        $dataMapper = new DataMapper($this->options, $serializer);

        $this->client = new Server(
            $dataMapper,
            new \GuzzleHttp\Client(),
            new ResponseParser(),
            $dispatcher,
            $logger
        );

        $this->notification = new Notification(
            $dataMapper,
            $dispatcher,
            $logger
        );
    }

    private function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'merchantId',
            'portalId',
            'key',
            'subAccountId'
        ]);
        $resolver->setDefaults([
            'apiVersion' => Server\Request::API_VERSION_3_11,
            'mode' => 'test',
            'encoding' => Server\Request::ENCODING_UTF8,
            'business' => false,
            'invoicing' => true,
        ]);
        $resolver->setAllowedTypes('merchantId', ['string']);
        $resolver->setAllowedTypes('portalId', ['string']);
        $resolver->setAllowedTypes('key', ['string']);
        $resolver->setAllowedTypes('subAccountId', ['string']);
        $resolver->setAllowedValues('apiVersion', [
            Server\Request::API_VERSION_3_8,
            Server\Request::API_VERSION_3_9,
            Server\Request::API_VERSION_3_10,
            Server\Request::API_VERSION_3_11,
        ]);
        $resolver->setAllowedValues('mode', [
            Server\Request::MODE_TEST,
            Server\Request::MODE_LIVE
        ]);
        $resolver->setAllowedValues('encoding', [
            Server\Request::ENCODING_UTF8,
            Server\Request::ENCODING_ISO88591
        ]);
    }

    /**
     * @return Notification
     */
    public function getNotification(): Notification
    {
        return $this->notification;
    }

    /**
     * @return Server
     */
    public function getServer(): Server
    {
        return $this->client;
    }
}