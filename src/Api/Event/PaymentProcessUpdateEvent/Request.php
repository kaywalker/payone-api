<?php

namespace Payone\Api\Event\PaymentProcessUpdateEvent;

use Payone\Api;

class Request extends Api\Event\PaymentProcessUpdateEvent
{
    /**
     * @var Api\Server\Request
     */
    protected $request;
    /**
     * @var Api\Server\Response
     */
    protected $response;

    /**
     * RequestEvent constructor.
     * @param \Payone\Api\Notification\Request $request
     * @param Api\Server\Response $response
     */
    public function __construct(Api\Server\Request $request, Api\Server\Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * @return Api\Server\Request
     */
    public function getRequest(): Api\Server\Request
    {
        return $this->request;
    }

    /**
     * @return Api\Server\Response
     */
    public function getResponse(): Api\Server\Response
    {
        return $this->response;
    }
}