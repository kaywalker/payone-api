<?php

namespace Payone\Api\Event\PaymentProcessUpdateEvent;

use Payone\Api\Event\PaymentProcessUpdateEvent;
use \Payone\Api\Notification;

class TransactionStatus extends PaymentProcessUpdateEvent
{
    /**
     * @var \Payone\Api\Notification\Request\TransactionStatus
     */
    protected $transactionStatus;

    /**
     * TransactionStatusEvent constructor.
     * @param \Payone\Api\Notification\Request\TransactionStatus $transactionStatus
     */
    public function __construct(Notification\Request\TransactionStatus $transactionStatus)
    {
        $this->transactionStatus = $transactionStatus;
    }

    /**
     * @return \Payone\Api\Notification\Request\TransactionStatus
     */
    public function getTransactionStatus(): Notification\Request\TransactionStatus
    {
        return $this->transactionStatus;
    }
}