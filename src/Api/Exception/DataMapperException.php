<?php

namespace Payone\Api\Exception;

class DataMapperException extends \Exception
{

    const CODE_KEY_ALLREADY_EXISTS = 1559554799;
    const CODE_RESPONSE_DATA_MUST_NOT_BE_EMPTY = 1559803640;
    const CODE_INVALID_RESPONSE_STATUS = 1559803899;
    const CODE_INVALID_NOTIFICATION_CREDENTIALS = 1560505638;
}