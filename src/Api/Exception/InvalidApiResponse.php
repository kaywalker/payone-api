<?php

namespace Payone\Api\Exception;

class InvalidApiResponse extends \Exception
{

    const CODE_INVALID_STATUS_CODE = 1559545131;
    const CODE_EMPTY_BODY = 1559545160;
}