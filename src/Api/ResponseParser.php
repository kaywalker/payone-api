<?php

namespace Payone\Api;

class ResponseParser
{
    /**
     * @var array
     */
    private $responseKeys = ['status', 'txid', 'userid', 'redirecturl', 'errorcode', 'errormessage', 'customermessage'];

    /**
     * @param string $content
     * @return array
     */
    public function parse(string $content): array
    {
        $result = [];

        $lines = preg_split('/[\n]+/', $content);
        foreach ($lines as $line) {
            foreach ($this->responseKeys as $key) {
                if (mb_substr($line, 0, mb_strlen($key)) === $key) {
                    $result[$key] = mb_substr($line, mb_strlen($key) + 1);
                }
            }
        }

        return $result;
    }
}