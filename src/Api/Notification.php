<?php

namespace Payone\Api;

use Payone\Api\Event\PaymentProcessUpdateEvent;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class Notification
{
    /**
     * @var DataMapper
     */
    private $dataMapper;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * Server constructor.
     * @param DataMapper $dataMapper
     * @param EventDispatcherInterface $dispatcher
     * @param LoggerInterface $logger
     */
    public function __construct(DataMapper $dataMapper, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        $this->dataMapper = $dataMapper;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    public function handleTransactionStatusRequest(ServerRequestInterface $request): ResponseInterface
    {

        $transactionStatusData = $request->getQueryParams();

        // payone sends ISO-8859-1 encoded values. convert to UTF-8
        array_walk_recursive(
            $transactionStatusData,
            function (&$value, $index) {
                $value = utf8_encode($value);
            }
        );

        $this->logger->info('handling transaction status request', ['transactionStatusData' => $transactionStatusData]);

        try {
            $transactionStatus = $this->dataMapper->mapTransactionStatusData($transactionStatusData);

            if ($transactionStatus !== null) {
                $this->dispatcher->dispatch(new PaymentProcessUpdateEvent\TransactionStatus($transactionStatus));
            }
        } catch (Exception\DataMapperException $e) {
            $this->logger->error($e->getMessage());
        }


        return new \GuzzleHttp\Psr7\Response(200, [], 'TSOK');
    }
}