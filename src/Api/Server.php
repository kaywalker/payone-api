<?php

namespace Payone\Api;

use GuzzleHttp\ClientInterface;
use Payone\Api;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class Server
{
    const PAYONE_SERVER_API_URL = 'https://api.pay1.de/post-gateway/';
    /**
     * @var DataMapper
     */
    private $dataMapper;
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;
    /**
     * @var ResponseParser
     */
    private $responseParser;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * Client constructor.
     *
     * @param DataMapper $dataMapper
     * @param ClientInterface $client
     * @param ResponseParser $responseParser
     * @param EventDispatcherInterface $dispatcher
     * @param LoggerInterface $logger
     */
    public function __construct(DataMapper $dataMapper, ClientInterface $client, ResponseParser $responseParser, EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        $this->dataMapper = $dataMapper;
        $this->client = $client;
        $this->responseParser = $responseParser;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    /**
     * @param Api\Server\Request $request
     * @return Api\Server\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws Api\Exception\InvalidApiResponse
     * @throws Api\Exception\DataMapperException
     */
    public function request(Api\Server\Request $request): Api\Server\Response
    {
        // map request to request data array
        $requestData = $this->dataMapper->mapRequest($request);

        // perform request
        $responseData = $this->makeApiRequest($requestData);

        // map response data array to response object
        $response = $this->dataMapper->mapResponse($responseData);

        $this->dispatcher->dispatch(new Api\Event\PaymentProcessUpdateEvent\Request($request, $response));

        return $response;
    }

    /**
     * @param array $requestData
     * @return array
     * @throws Api\Exception\InvalidApiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function makeApiRequest(array $requestData): array
    {
        $response = $this->client->request('POST', self::PAYONE_SERVER_API_URL, ['form_params' => $requestData]);

        $this->logger->info('api request', ['requestData' => $requestData]);

        $statusCode = $response->getStatusCode();

        if ($statusCode !== 200) {
            throw new Api\Exception\InvalidApiResponse(sprintf('Api responded with status code "%s"', $statusCode), Api\Exception\InvalidApiResponse::CODE_INVALID_STATUS_CODE);
        }

        $contents = $response->getBody()->getContents();
        if ($contents === '') {
            throw new Api\Exception\InvalidApiResponse('Api responded with empty body', Api\Exception\InvalidApiResponse::CODE_EMPTY_BODY);
        }

        // parse response content to response data array
        $responseData = $this->responseParser->parse($contents);

        $this->logger->info('api response', ['responseData' => $responseData]);


        return $responseData;
    }
}