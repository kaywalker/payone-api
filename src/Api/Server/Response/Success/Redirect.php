<?php

namespace Payone\Api\Server\Response\Success;

use Payone\Api\Server\Response\Success;
use JMS\Serializer\Annotation as Serializer;

class Redirect extends Success
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("redirecturl")
     */
    private $url;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}