<?php

namespace Payone\Api\Server\Response;

use JMS\Serializer\Annotation as Serializer;

class Clearing
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_bankaccountholder")
     */
    private $bankAccountHolder;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_bankcountry")
     */
    private $bankCountry;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_bankaccount")
     */
    private $bankAccountNumber;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_bankcode")
     */
    private $bankCode;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_bankiban")
     */
    private $bankIban;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_bankbic")
     */
    private $bankBic;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_bankcity")
     */
    private $bankCity;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_bankname")
     */
    private $bankName;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("clearing_instructionnote")
     */
    private $instructionNote;

    /**
     * @return string
     */
    public function getBankAccountHolder(): string
    {
        return $this->bankAccountHolder;
    }

    /**
     * @return string
     */
    public function getBankCountry(): string
    {
        return $this->bankCountry;
    }

    /**
     * @return string
     */
    public function getBankAccountNumber(): string
    {
        return $this->bankAccountNumber;
    }

    /**
     * @return string
     */
    public function getBankCode(): string
    {
        return $this->bankCode;
    }

    /**
     * @return string
     */
    public function getBankIban(): string
    {
        return $this->bankIban;
    }

    /**
     * @return string
     */
    public function getBankBic(): string
    {
        return $this->bankBic;
    }

    /**
     * @return string
     */
    public function getBankCity(): string
    {
        return $this->bankCity;
    }

    /**
     * @return string
     */
    public function getBankName(): string
    {
        return $this->bankName;
    }

    /**
     * @return string
     */
    public function getInstructionNote(): string
    {
        return $this->instructionNote;
    }
}