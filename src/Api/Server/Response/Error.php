<?php

namespace Payone\Api\Server\Response;

use Payone\Api\Server\Response;
use JMS\Serializer\Annotation as Serializer;

class Error extends Response
{
    /**
     * @var int
     *
     * @Serializer\Type("int"))
     * @Serializer\SerializedName("errorcode")
     */
    private $errorCode;
    /**
     * @var string
     *
     * @Serializer\Type("string"))
     * @Serializer\SerializedName("errormessage")
     */
    private $errorMessage;
    /**
     * @var string
     * @Serializer\Type("string"))
     * @Serializer\SerializedName("customermessage")
     */
    private $customerMessage;
}