<?php

namespace Payone\Api\Server\Response;

use Payone\Api\Server\Response;
use JMS\Serializer\Annotation as Serializer;

abstract class Success extends Response
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("txid")
     */
    private $paymentProcessId;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("userid")
     */
    private $userId;
    /**
     * @var Clearing
     */
    private $clearing;

    /**
     * @return string
     */
    public function getPaymentProcessId(): string
    {
        return $this->paymentProcessId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return Clearing
     */
    public function getClearing(): Clearing
    {
        return $this->clearing;
    }

    /**
     * @param Clearing $clearing
     */
    public function setClearing(Clearing $clearing): void
    {
        $this->clearing = $clearing;
    }
}