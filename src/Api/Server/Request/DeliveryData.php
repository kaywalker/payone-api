<?php

namespace Payone\Api\Server\Request;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DeliveryData
{
    /**
     * @var string
     *
     * @Serializer\SerializedName("shipping_firstname")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $firstname;
    /**
     * @var string
     *
     * @Serializer\SerializedName("shipping_lastname")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $lastname;
    /**
     * @var string
     *
     * @Serializer\SerializedName("shipping_company")
     *
     * @Assert\Length(min = 2, max = 50)
     * @Assert\Type(type="alnum")
     */
    private $company;
    /**
     * @var string
     *
     * @Serializer\SerializedName("shipping_street")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $street;
    /**
     * @var string
     *
     * @Serializer\SerializedName("shipping_zip")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 10)
     * @Assert\Regex("/^[a-zA-Z0-9_\.\-\/ ]{2,10}$/")
     */
    private $zip;
    /**
     * @var string
     *
     * @Serializer\SerializedName("shipping_city")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $city;
    /**
     * ISO 3166
     *
     * @var string
     *
     * @Serializer\SerializedName("shipping_country")
     *
     * @Assert\NotBlank()
     * @Assert\Country()
     */
    private $country;
    /**
     * ISO 3166-2 subdivisions
     *
     * @var string
     *
     * @Serializer\SerializedName("shipping_state")
     *
     * Todo improve validation
     */
    private $state;

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company): void
    {
        $this->company = $company;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @param string $zip
     */
    public function setZip(string $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }
}