<?php

namespace Payone\Api\Server\Request;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class Item
{
    const TYPE_GOODS = 'goods';
    const TYPE_SHIPMENT = 'shipment';
    const TYPE_HANDLING = 'handling';
    const TYPE_VOUCHER = 'voucher';
    const TYPES = [
        self::TYPE_GOODS,
        self::TYPE_SHIPMENT,
        self::TYPE_HANDLING,
        self::TYPE_VOUCHER
    ];
    /**
     * @var string
     *
     * @Serializer\SerializedName("it")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Payone\Api\Server\Request\Item::TYPES)
     */
    private $type;
    /**
     * @var string
     *
     * @Serializer\SerializedName("id")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 32)
     * @Assert\Regex("/^[0-9a-zA-Z\(\)\[\]\{\}\+\-_\#\/\:]+$/")
     */
    private $id;
    /**
     * @var int
     *
     * @Serializer\SerializedName("pr")
     *
     * @Assert\NotBlank()
     * @Assert\Range(min = 1, max = 1999999999)
     */
    private $unitPrice;
    /**
     * @var int
     *
     * @Serializer\SerializedName("no")
     *
     * @Assert\NotBlank()
     * @Assert\Range(min = 1, max = 999999)
     */
    private $quantity;
    /**
     * @var string
     *
     * @Serializer\SerializedName("de")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 255)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $description;
    /**
     * @var int
     *
     * @Serializer\SerializedName("va")
     *
     * @Assert\Range(min = 1, max = 99)
     */
    private $vat;
    /**
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'Ymd'>")
     * @Serializer\SerializedName("sd")
     */
    private $deliveryDate;
    /**
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'Ymd'>")
     * @Serializer\SerializedName("ed")
     */
    private $deliveryPeriodEndDate;

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @param int $unitPrice
     */
    public function setUnitPrice(int $unitPrice): void
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param int $vat
     */
    public function setVat(int $vat): void
    {
        $this->vat = $vat;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate(\DateTime $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @param \DateTime $deliveryPeriodEndDate
     */
    public function setDeliveryPeriodEndDate(\DateTime $deliveryPeriodEndDate): void
    {
        $this->deliveryPeriodEndDate = $deliveryPeriodEndDate;
    }
}