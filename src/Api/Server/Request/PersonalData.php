<?php

namespace Payone\Api\Server\Request;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class PersonalData
{
    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';
    const GENDERS = [self::GENDER_MALE, self::GENDER_FEMALE];
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("customerid")
     *
     * @Assert\Length(min = 1, max = 20)
     * @Assert\Regex("/^[0-9a-zA-Z\.\-_\/]+$/"))
     */
    private $customerId;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("userid")
     *
     * @Assert\Length(min = 6, max = 12)
     * @Assert\Type(type="digit")
     */
    private $userId;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("salutation")
     *
     * @Assert\Length(min = 1, max = 10)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $salutation;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("title")
     *
     * @Assert\Length(min = 1, max = 20)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $title;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("firstname")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $firstname;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("lastname")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $lastname;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("company")
     *
     * @Assert\Length(min = 2, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $company;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("street")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $street;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("addressaddition")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $addressAddition;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("zip")
     *
     * @Assert\Length(min = 2, max = 10)
     * @Assert\Regex("/^[a-zA-Z0-9_\.\-\/ ]{2,10}$/")
     */
    private $zip;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("city")
     *
     * @Assert\Length(min = 2, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $city;
    /**
     * ISO 3166
     *
     * @var string|null
     *
     * @Serializer\SerializedName("country")
     *
     * @Assert\NotBlank()
     * @Assert\Country()
     */
    private $country;
    /**
     * ISO 3166-2 subdivisions
     *
     * @var string|null
     *
     * @Serializer\SerializedName("state")
     *
     * Todo improve validation
     */
    private $state;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("email")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Email()
     */
    private $email;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("telephonenumber")
     *
     * @Assert\Length(min = 1, max = 30)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $telephoneNumber;
    /**
     * @var \DateTime|null
     *
     * @Serializer\Type("DateTime<'Ymd'>")
     * @Serializer\SerializedName("birthday")
     */
    private $birthday;
    /**
     * ISO 639
     *
     * @var string|null
     *
     * @Serializer\SerializedName("language")
     *
     * @Assert\Language()
     */
    private $language;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("vatid")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $vatId;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("gender")
     *
     * @Assert\Choice(choices=Payone\Api\Server\Request\PersonalData::GENDERS)
     */
    private $gender;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("personalid")
     *
     * @Assert\Length(min = 1, max = 32)
     * @Assert\Regex("/^[0-9a-zA-Z\+\-\.\/\(\)]+$/"))
     */
    private $personalId;
    /**
     * @var string|null
     *
     * @Serializer\SerializedName("ip")
     *
     * @Assert\Length(min = 1, max = 39)
     * @Assert\Ip(version = "all")
     */
    private $ip;

    /**
     * @param string $customerId
     */
    public function setCustomerId(string $customerId): void
    {
        $this->customerId = $customerId;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @param string $salutation
     */
    public function setSalutation(string $salutation): void
    {
        $this->salutation = $salutation;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company): void
    {
        $this->company = $company;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @param string $addressAddition
     */
    public function setAddressAddition(string $addressAddition): void
    {
        $this->addressAddition = $addressAddition;
    }

    /**
     * @param string $zip
     */
    public function setZip(string $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @param string $telephoneNumber
     */
    public function setTelephoneNumber(string $telephoneNumber): void
    {
        $this->telephoneNumber = $telephoneNumber;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday(\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    /**
     * @param string $vatId
     */
    public function setVatId(string $vatId): void
    {
        $this->vatId = $vatId;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @param string $personalId
     */
    public function setPersonalId(string $personalId): void
    {
        $this->personalId = $personalId;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip): void
    {
        $this->ip = $ip;
    }
}