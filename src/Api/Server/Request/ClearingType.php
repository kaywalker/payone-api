<?php

namespace Payone\Api\Server\Request;

use Payone\Api\Server\Request;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

abstract class ClearingType
{
    const CLEARING_TYPE_CREDIT_CARD = 'cc';
    const CLEARING_TYPE_EWALLET = 'wlt';
    const CLEARING_TYPE_ONLINE_BANK_TRANSFER = 'sb';
    const CLEARING_TYPE_PREPAYMENT = 'vor';
    /**
     * @var string
     *
     * @Serializer\SerializedName("clearingtype")
     *
     * @Assert\NotBlank()
     */
    protected $type;
}