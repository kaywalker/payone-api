<?php

namespace Payone\Api\Server\Request;

use JMS\Serializer\Annotation as Serializer;
use Payone\Api\Server\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

abstract class BaseAuthorization extends Request
{
    const REQUEST_TYPE_TO_CLEARING_TYPES = [
        Request\Preauthorization::class => [
            Request\ClearingType\CreditCard::class,
            Request\ClearingType\CreditCardWithPseudoCardPan::class,
            Request\ClearingType\EWallet\PayPalExpress::class,
            Request\ClearingType\OnlineBankTransfer\SofortBanking::class,
            Request\ClearingType\Prepayment::class
        ],
        Request\Authorization::class => [
            Request\ClearingType\CreditCard::class,
            Request\ClearingType\CreditCardWithPseudoCardPan::class,
            Request\ClearingType\EWallet\PayPalExpress::class,
            Request\ClearingType\OnlineBankTransfer\SofortBanking::class,
        ]
    ];
    /**
     * @var string
     *
     * @Serializer\SerializedName("reference")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 20)
     * @Assert\Regex("/^[0-9a-zA-Z\.\-_\/]+$/"))
     */
    private $reference;
    /**
     * @var int
     *
     * @Serializer\SerializedName("amount")
     *
     * @Assert\NotBlank()
     * @Assert\Range(min = 1, max = 1999999999)
     */
    private $amount;
    /**
     * ISO 4217
     *
     * @var string
     *
     * @Serializer\SerializedName("currency")
     *
     * @Assert\NotBlank()
     * @Assert\Currency()
     */
    private $currency;
    /**
     * @var string
     *
     * @Serializer\SerializedName("param")
     *
     * @Assert\Length(min = 1, max = 255)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $individualParameter;
    /**
     * @var string
     *
     * @Serializer\SerializedName("narrative_text")
     *
     * @Assert\Length(min = 1, max = 81)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $narrativeText;
    /**
     * @var ClearingType
     *
     * @Assert\NotNull
     * @Assert\Valid()
     */
    private $clearingType;
    /**
     * @var PersonalData
     *
     * @Assert\Valid()
     */
    private $personalData;
    /**
     * @var DeliveryData
     *
     * @Assert\Valid()
     */
    private $deliveryData;
    /**
     * @var array
     *
     * @Serializer\Type("array<Payone\Api\Server\Request\Item>")
     *
     * @Assert\Valid(traverse=true)
     */
    private $items;

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->clearingType !== null) {
            $requestClassname = static::class;
            $clearingTypeClassname = get_class($this->clearingType);

            $allowedClearingTypes = self::REQUEST_TYPE_TO_CLEARING_TYPES[$requestClassname];

            if (!in_array($clearingTypeClassname, $allowedClearingTypes)) {
                $context->buildViolation('The clearing type "%clearingTypeClassname%" is not allowed for request of type "%requestClassname%"')
                    ->setParameter('%clearingTypeClassname%', $clearingTypeClassname)
                    ->setParameter('%requestClassname%', $requestClassname)
                    ->atPath('clearingType')
                    ->addViolation();
            };
        }
    }

    /**
     * @param ClearingType $clearingType
     */
    public function setClearingType(ClearingType $clearingType): void
    {
        $this->clearingType = $clearingType;
    }

    /**
     * @param string $reference
     */
    public function setReference(string $reference): void
    {
        $this->reference = $reference;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @param string $individualParameter
     */
    public function setIndividualParameter(string $individualParameter): void
    {
        $this->individualParameter = $individualParameter;
    }

    /**
     * @param string $narrativeText
     */
    public function setNarrativeText(string $narrativeText): void
    {
        $this->narrativeText = $narrativeText;
    }

    /**
     * @param PersonalData $personalData
     */
    public function setPersonalData(PersonalData $personalData): void
    {
        $this->personalData = $personalData;
    }

    /**
     * @param DeliveryData $deliveryData
     */
    public function setDeliveryData(DeliveryData $deliveryData): void
    {
        $this->deliveryData = $deliveryData;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }
}