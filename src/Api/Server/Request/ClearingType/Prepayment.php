<?php

namespace Payone\Api\Server\Request\ClearingType;

use Payone\Api\Server\Request\ClearingType;

class Prepayment extends ClearingType
{
    /**
     * Prepayment constructor.
     */
    public function __construct()
    {
        $this->type = self::CLEARING_TYPE_PREPAYMENT;
    }
}