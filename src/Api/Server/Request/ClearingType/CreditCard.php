<?php

namespace Payone\Api\Server\Request\ClearingType;

use JMS\Serializer\Annotation as Serializer;
use Payone\Api\Server\Request\ClearingType;
use Symfony\Component\Validator\Constraints as Assert;

class CreditCard extends ClearingType
{
    const CARD_TYPE_VISA = 'V';
    const CARD_TYPES = [
        self::CARD_TYPE_VISA
    ];
    const ECOMMERCE_MODE_INTERNET = 'internet';
    const ECOMMERCE_MODE_3DSECURE = '3dsecure';
    const ECOMMERCE_MODE_MOTO = 'moto';
    const ECOMMERCE_MODES = [
        self::ECOMMERCE_MODE_INTERNET,
        self::ECOMMERCE_MODE_3DSECURE,
        self::ECOMMERCE_MODE_MOTO
    ];
    /**
     * @var string
     *
     * @Serializer\SerializedName("cardpan")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 19)
     * @Assert\Type(type="digit")
     */
    private $cardPan;
    /**
     * @var string
     *
     * @Serializer\SerializedName("cardtype")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Payone\Api\Server\Request\ClearingType\CreditCard::CARD_TYPES)
     */
    private $cardType;
    /**
     * @var string
     *
     * @Serializer\SerializedName("cardexpiredate")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 4, max = 4)
     * @Assert\Type(type="digit")
     */
    private $cardExpireDate;
    /**
     * @var string
     *
     * @Serializer\SerializedName("cardcvc2")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 4)
     * @Assert\Type(type="digit")
     */
    private $cardValidationCode;
    /**
     * @var string
     *
     * @Serializer\SerializedName("cardissuenumber")
     *
     * @Assert\Length(min = 1, max = 2)
     * @Assert\Type(type="digit")
     */
    private $cardIssueNumber;
    /**
     * @var string
     *
     * @Serializer\SerializedName("cardholder")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type(type="alnum")
     */
    private $cardHolder;
    /**
     * @var string
     *
     * @Serializer\SerializedName("ecommercemode")
     *
     * @Assert\Choice(choices=Payone\Api\Server\Request\ClearingType\CreditCard::ECOMMERCE_MODES)
     */
    private $ecommerceMode;

    /**
     * CreditCard constructor.
     */
    public function __construct()
    {
        $this->type = self::CLEARING_TYPE_CREDIT_CARD;
    }

    /**
     * @param string $cardPan
     */
    public function setCardPan(string $cardPan): void
    {
        $this->cardPan = $cardPan;
    }

    /**
     * @param string $cardType
     */
    public function setCardType(string $cardType): void
    {
        $this->cardType = $cardType;
    }

    /**
     * @param string $cardExpireDate
     */
    public function setCardExpireDate(string $cardExpireDate): void
    {
        $this->cardExpireDate = $cardExpireDate;
    }

    /**
     * @param string $cardValidationCode
     */
    public function setCardValidationCode(string $cardValidationCode): void
    {
        $this->cardValidationCode = $cardValidationCode;
    }

    /**
     * @param string $cardIssueNumber
     */
    public function setCardIssueNumber(string $cardIssueNumber): void
    {
        $this->cardIssueNumber = $cardIssueNumber;
    }

    /**
     * @param string $cardHolder
     */
    public function setCardHolder(string $cardHolder): void
    {
        $this->cardHolder = $cardHolder;
    }

    /**
     * @param string $ecommerceMode
     */
    public function setEcommerceMode(string $ecommerceMode): void
    {
        $this->ecommerceMode = $ecommerceMode;
    }
}