<?php

namespace Payone\Api\Server\Request\ClearingType;

use JMS\Serializer\Annotation as Serializer;
use Payone\Api\Server\Request\ClearingType;
use Symfony\Component\Validator\Constraints as Assert;

abstract class OnlineBankTransfer extends ClearingType
{
    const ONLINE_BANK_TRANSFER_TYPE_SOFORT_BANKING = 'PNT';
    const ONLINE_BANK_TRANSFER_TYPES = [
        self::ONLINE_BANK_TRANSFER_TYPE_SOFORT_BANKING
    ];
    /**
     * @var string
     *
     * @Serializer\SerializedName("onlinebanktransfertype")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Payone\Api\Server\Request\ClearingType\OnlineBankTransfer::ONLINE_BANK_TRANSFER_TYPES)
     */
    protected $onlineBankTransferType;

    /**
     * CreditCard constructor.
     */
    public function __construct()
    {
        $this->type = self::CLEARING_TYPE_ONLINE_BANK_TRANSFER;
    }
}