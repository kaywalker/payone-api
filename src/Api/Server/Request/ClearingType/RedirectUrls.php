<?php

namespace Payone\Api\Server\Request\ClearingType;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

trait RedirectUrls
{
    /**
     * @var string
     *
     * @Serializer\SerializedName("successurl")
     *
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $sucessUrl;
    /**
     * @var string
     *
     * @Serializer\SerializedName("errorurl")
     *
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $errorUrl;
    /**
     * @var string
     *
     * @Serializer\SerializedName("backurl")
     *
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $backUrl;

    /**
     * @param string $sucessUrl
     */
    public function setSucessUrl(string $sucessUrl): void
    {
        $this->sucessUrl = $sucessUrl;
    }

    /**
     * @param string $errorUrl
     */
    public function setErrorUrl(string $errorUrl): void
    {
        $this->errorUrl = $errorUrl;
    }

    /**
     * @param string $backUrl
     */
    public function setBackUrl(string $backUrl): void
    {
        $this->backUrl = $backUrl;
    }
}