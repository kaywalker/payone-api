<?php

namespace Payone\Api\Server\Request\ClearingType;

use JMS\Serializer\Annotation as Serializer;
use Payone\Api\Server\Request\ClearingType;
use Symfony\Component\Validator\Constraints as Assert;

abstract class EWallet extends ClearingType
{
    const WALLET_TYPE_PPE = 'PPE';
    const WALLET_TYPES = [
        self::WALLET_TYPE_PPE
    ];
    /**
     * @var string
     *
     * @Serializer\SerializedName("wallettype")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Payone\Api\Server\Request\ClearingType\EWallet::WALLET_TYPES)
     */
    protected $walletType;

    /**
     * EWallet constructor.
     */
    public function __construct()
    {
        $this->type = self::CLEARING_TYPE_EWALLET;
    }
}