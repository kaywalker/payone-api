<?php

namespace Payone\Api\Server\Request\ClearingType\OnlineBankTransfer;

use JMS\Serializer\Annotation as Serializer;
use Payone\Api\Server\Request\ClearingType\OnlineBankTransfer;
use Payone\Api\Server\Request\ClearingType\RedirectUrls;
use Symfony\Component\Validator\Constraints as Assert;

class SofortBanking extends OnlineBankTransfer
{
    use RedirectUrls;
    /**
     * @var string
     *
     * @Serializer\SerializedName("bankcountry")
     *
     * @Assert\NotBlank()
     */
    private $bankCountry;

    /**
     * SofortBanking constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->onlineBankTransferType = self::ONLINE_BANK_TRANSFER_TYPE_SOFORT_BANKING;
    }

    /**
     * @param string $bankCountry
     */
    public function setBankCountry(string $bankCountry): void
    {
        $this->bankCountry = $bankCountry;
    }
}