<?php

namespace Payone\Api\Server\Request\ClearingType;

use JMS\Serializer\Annotation as Serializer;
use Payone\Api\Server\Request\ClearingType;
use Symfony\Component\Validator\Constraints as Assert;

class CreditCardWithPseudoCardPan extends ClearingType
{
    /**
     * @var string
     *
     * @Serializer\SerializedName("pseudocardpan")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 19)
     * @Assert\Type(type="digit")
     */
    private $pseudoCardPan;

    /**
     * CreditCard constructor.
     */
    public function __construct()
    {
        $this->type = self::CLEARING_TYPE_CREDIT_CARD;
    }

    /**
     * @param string $pseudoCardPan
     */
    public function setPseudoCardPan(string $pseudoCardPan): void
    {
        $this->pseudoCardPan = $pseudoCardPan;
    }
}