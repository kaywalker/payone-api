<?php

namespace Payone\Api\Server\Request\ClearingType\EWallet;

use Payone\Api\Server\Request\ClearingType\EWallet;
use Payone\Api\Server\Request\ClearingType\RedirectUrls;

class PayPalExpress extends EWallet
{
    use RedirectUrls;

    /**
     * PayPalExpress constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->walletType = self::WALLET_TYPE_PPE;
    }
}