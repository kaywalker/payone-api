<?php

namespace Payone\Api\Server\Request;

class Preauthorization extends BaseAuthorization
{
    /**
     * Preauthorization constructor.
     */
    public function __construct()
    {
        $this->setRequest(self::REQUEST_TYPE_PREAUTHORIZATION);
    }
}