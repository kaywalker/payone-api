<?php

namespace Payone\Api\Server\Request;

use JMS\Serializer\Annotation as Serializer;

class Authorization extends BaseAuthorization
{
    /**
     * @var string
     * @Serializer\SerializedName("settleaccount")
     * @Serializer\Groups({"business"})
     */
    private $settleAccount;
    /**
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'Ymd'>")
     * @Serializer\SerializedName("document_date")
     * @Serializer\Groups({"business"})
     */
    private $documentDate;
    /**
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'Ymd'>")
     * @Serializer\SerializedName("booking_date")
     * @Serializer\Groups({"business"})
     */
    private $bookingDate;
    /**
     * @var \DateTime
     *
     * @Serializer\Type("DateTime<'U'>")
     * @Serializer\SerializedName("due_time")
     * @Serializer\Groups({"business"})
     */
    private $dueTime;

    /**
     * Authorization constructor.
     */
    public function __construct()
    {
        $this->setRequest(self::REQUEST_TYPE_AUTHORIZATION);
    }

    /**
     * @param string $settleAccount
     */
    public function setSettleAccount(string $settleAccount): void
    {
        $this->settleAccount = $settleAccount;
    }

    /**
     * @param \DateTime $documentDate
     */
    public function setDocumentDate(\DateTime $documentDate): void
    {
        $this->documentDate = $documentDate;
    }

    /**
     * @param \DateTime $bookingDate
     */
    public function setBookingDate(\DateTime $bookingDate): void
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @param \DateTime $dueTime
     */
    public function setDueTime(\DateTime $dueTime): void
    {
        $this->dueTime = $dueTime;
    }
}