<?php

namespace Payone\Api\Server;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

abstract class Request
{
    const API_VERSION_3_8 = '3.8';
    const API_VERSION_3_9 = '3.9';
    const API_VERSION_3_10 = '3.10';
    const API_VERSION_3_11 = '3.11';
    const API_VERSIONS = [
        self::API_VERSION_3_8,
        self::API_VERSION_3_9,
        self::API_VERSION_3_10,
        self::API_VERSION_3_11
    ];
    const MODE_TEST = 'test';
    const MODE_LIVE = 'live';
    const MODES = [
        self::MODE_TEST,
        self::MODE_LIVE
    ];
    const REQUEST_TYPE_AUTHORIZATION = 'authorization';
    const REQUEST_TYPE_PREAUTHORIZATION = 'preauthorization';
    const REQUEST_TYPE_CREDIT_CARD_CHECK = 'creditcardcheck';
    const REQUEST_TYPES = [
        self::REQUEST_TYPE_AUTHORIZATION,
        self::REQUEST_TYPE_PREAUTHORIZATION
    ];
    const ENCODING_ISO88591 = 'ISO-8859-1';
    const ENCODING_UTF8 = 'UTF-8';
    const ENCODINGS = [
        self::ENCODING_ISO88591,
        self::ENCODING_UTF8
    ];
    /**
     * @var string
     *
     * @Serializer\SerializedName("request")
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Payone\Api\Server\Request::REQUEST_TYPES)
     */
    private $request;

    /**
     * @return string
     */
    public function getRequest(): string
    {
        return $this->request;
    }

    /**
     * @param string $request
     */
    public function setRequest(string $request): void
    {
        $this->request = $request;
    }
}