<?php

namespace Payone\Api\Server;

abstract class Response
{
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_PENDING = 'PENDING';
    const STATUS_REDIRECT = 'REDIRECT';
    const STATUS_ERROR = 'ERROR';
}