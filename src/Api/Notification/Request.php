<?php

namespace Payone\Api\Notification;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

abstract class Request
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("key")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 32)
     * @Assert\Type(type="alnum")
     */
    protected $key;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("txaction")
     */
    protected $action;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("transaction_status")
     */
    protected $transactionStatus;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("notify_version")
     */
    protected $notifyVersion;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("mode")
     *
     * @Assert\NotBlank()
     * @Assert\Choice(choices=Payone\Api\Server\Request::MODES)
     */
    protected $mode;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("portalid")
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 7)
     * @Assert\Type(type="digit")
     */
    protected $portalId;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("aid")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 6)
     * @Assert\Type(type="digit")
     */
    protected $subAccountId;

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getTransactionStatus(): string
    {
        return $this->transactionStatus;
    }

    /**
     * @param string $transactionStatus
     */
    public function setTransactionStatus(string $transactionStatus): void
    {
        $this->transactionStatus = $transactionStatus;
    }

    /**
     * @return string
     */
    public function getNotifyVersion(): string
    {
        return $this->notifyVersion;
    }

    /**
     * @param string $notifyVersion
     */
    public function setNotifyVersion(string $notifyVersion): void
    {
        $this->notifyVersion = $notifyVersion;
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function getPortalId(): string
    {
        return $this->portalId;
    }

    /**
     * @param string $portalId
     */
    public function setPortalId(string $portalId): void
    {
        $this->portalId = $portalId;
    }

    /**
     * @return string
     */
    public function getSubAccountId(): string
    {
        return $this->subAccountId;
    }

    /**
     * @param string $subAccountId
     */
    public function setSubAccountId(string $subAccountId): void
    {
        $this->subAccountId = $subAccountId;
    }
}