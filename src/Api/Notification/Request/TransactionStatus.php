<?php

namespace Payone\Api\Notification\Request;

use Payone\Api\Notification;
use Payone\Api\Notification\Request;
use Payone\Api\Notification\Request\DeliveryData;
use Payone\Api\Notification\Request\PersonalData;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class TransactionStatus extends Request
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("txid")
     */
    private $paymentProcessId;
    /**
     * @var PersonalData
     *
     * @Assert\Valid()
     */
    private $personalData;
    /**
     * @var DeliveryData
     *
     * @Assert\Valid()
     */
    private $deliveryData;

    /**
     * @return string
     */
    public function getPaymentProcessId(): string
    {
        return $this->paymentProcessId;
    }

    /**
     * @return PersonalData
     */
    public function getPersonalData(): PersonalData
    {
        return $this->personalData;
    }

    /**
     * @param PersonalData $personalData
     */
    public function setPersonalData(PersonalData $personalData): void
    {
        $this->personalData = $personalData;
    }

    /**
     * @return DeliveryData
     */
    public function getDeliveryData(): DeliveryData
    {
        return $this->deliveryData;
    }

    /**
     * @param DeliveryData $deliveryData
     */
    public function setDeliveryData(DeliveryData $deliveryData): void
    {
        $this->deliveryData = $deliveryData;
    }
}