<?php

namespace Payone\Api\Notification\Request;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class PersonalData
{
    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("firstname")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $firstname;
    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("lastname")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $lastname;
    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("company")
     *
     * @Assert\Length(min = 2, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $company;
    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("street")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $street;
    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("zip")
     *
     * @Assert\Length(min = 2, max = 10)
     * @Assert\Regex("/^[a-zA-Z0-9_\.\-\/ ]{2,10}$/")
     */
    private $zip;
    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("city")
     *
     * @Assert\Length(min = 2, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $city;
    /**
     * ISO 3166
     *
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("country")
     *
     * @Assert\NotBlank()
     * @Assert\Country()
     */
    private $country;

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     */
    public function setFirstname(?string $firstname): void
    {
        $this->firstname = $firstname;
    }
}