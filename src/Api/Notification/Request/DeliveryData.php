<?php

namespace Payone\Api\Notification\Request;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DeliveryData
{
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("shipping_firstname")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $firstname;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("shipping_lastname")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $lastname;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("shipping_company")
     *
     * @Assert\Length(min = 2, max = 50)
     * @Assert\Type(type="alnum")
     */
    private $company;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("shipping_street")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Type("string")
     *
     * Todo improve validation of allowed characters
     */
    private $street;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("shipping_zip")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 10)
     * @Assert\Regex("/^[a-zA-Z0-9_\.\-\/ ]{2,10}$/")
     */
    private $zip;
    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("shipping_city")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 50)
     * @Assert\Type(type="string")
     *
     * Todo improve validation of allowed characters
     */
    private $city;
    /**
     * ISO 3166
     *
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("shipping_country")
     *
     * @Assert\NotBlank()
     * @Assert\Country()
     */
    private $country;
    /**
     * @var string|null
     *
     * @Serializer\Type("string")
     * @Serializer\SerializedName("email")
     *
     * @Assert\Length(min = 1, max = 50)
     * @Assert\Email()
     */
    private $email;

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company): void
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip(string $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }
}