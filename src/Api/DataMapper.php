<?php

namespace Payone\Api;

use Payone\Api;
use JMS\Serializer\ArrayTransformerInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DataMapper
{
    /**
     * @var ArrayTransformerInterface
     */
    private $arrayTransformer;

    /**
     * DataMapper constructor.
     * @param ArrayTransformerInterface $arrayTransformer
     * @param bool $business
     */
    public function __construct(array $options, ArrayTransformerInterface $arrayTransformer)
    {
        $this->arrayTransformer = $arrayTransformer;

        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        $this->options = $resolver->resolve($options);
    }

    private function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'merchantId',
            'portalId',
            'key',
            'subAccountId'
        ]);
        $resolver->setDefaults([
            'apiVersion' => Api\Server\Request::API_VERSION_3_11,
            'mode' => 'test',
            'encoding' => Api\Server\Request::ENCODING_UTF8,
            'business' => false,
            'invoicing' => true,
        ]);
        $resolver->setAllowedTypes('merchantId', ['string']);
        $resolver->setAllowedTypes('portalId', ['string']);
        $resolver->setAllowedTypes('key', ['string']);
        $resolver->setAllowedTypes('subAccountId', ['string']);
        $resolver->setAllowedValues('apiVersion', [
            Api\Server\Request::API_VERSION_3_8,
            Api\Server\Request::API_VERSION_3_9,
            Api\Server\Request::API_VERSION_3_10,
            Api\Server\Request::API_VERSION_3_11,
        ]);
        $resolver->setAllowedValues('mode', [
            Api\Server\Request::MODE_TEST,
            Api\Server\Request::MODE_LIVE
        ]);
        $resolver->setAllowedValues('encoding', [
            Api\Server\Request::ENCODING_UTF8,
            Api\Server\Request::ENCODING_ISO88591
        ]);
    }

    public function getOption(string $name)
    {
        return $this->options[$name];
    }

    /**
     * @param Api\Server\Request $request
     * @return array
     * @throws Api\Exception\DataMapperException
     */
    public function mapRequest(Api\Server\Request $request)
    {
        $data = $this->arrayTransformer->toArray($request, $this->createContext());

        // "embed" items array into data
        if (array_key_exists('items', $data)) {
            $items = $data['items'];
            unset ($data['items']);

            foreach ($items as $index => $item) {
                foreach ($item as $key => $value) {
                    $this->addKeyValueToData($data, sprintf('%s[%s]', $key, $index + 1), $value);
                }
            }
        }

        // "embed" clearing type into data
        if (array_key_exists('clearing_type', $data)) {
            $clearingType = $data['clearing_type'];
            unset ($data['clearing_type']);

            foreach ($clearingType as $key => $value) {
                $this->addKeyValueToData($data, $key, $value);
            }
        }

        // "embed" personal data into data
        if (array_key_exists('personal_data', $data)) {
            $personalData = $data['personal_data'];
            unset ($data['personal_data']);

            foreach ($personalData as $key => $value) {
                $this->addKeyValueToData($data, $key, $value);
            }
        }

        // "embed" delivery data into data
        if (array_key_exists('delivery_data', $data)) {
            $deliveryData = $data['delivery_data'];
            unset ($data['delivery_data']);

            foreach ($deliveryData as $key => $value) {
                $this->addKeyValueToData($data, $key, $value);
            }
        }

        $data['mid'] = $this->getOption('merchantId');
        $data['portalid'] = $this->getOption('portalId');
        $data['key'] = hash('md5', $this->getOption('key'));
        $data['aid'] = $this->getOption('subAccountId');
        $data['api_version'] = $this->getOption('apiVersion');
        $data['mode'] = $this->getOption('mode');
        $data['encoding'] = $this->getOption('encoding');

        return $data;
    }

    /**
     * @param array $responseData
     * @return Api\Server\Response
     * @throws Api\Exception\DataMapperException
     */
    public function mapResponse(array $responseData)
    {
        if (empty($responseData)) {
            throw new Api\Exception\DataMapperException('response data must not be empty', Api\Exception\DataMapperException::CODE_RESPONSE_DATA_MUST_NOT_BE_EMPTY);
        }

        $response = null;

        $status = $responseData['status'];
        unset($responseData['status']);

        switch ($status) {
            case Api\Server\Response::STATUS_APPROVED:
                $response = $this->fromArray($responseData, Api\Server\Response\Success\Approved::class);
                break;
            case Api\Server\Response::STATUS_REDIRECT:
                $response = $this->fromArray($responseData, Api\Server\Response\Success\Redirect::class);
                break;
            case Api\Server\Response::STATUS_PENDING:
                $response = $this->fromArray($responseData, Api\Server\Response\Success\Pending::class);
                break;
            case Api\Server\Response::STATUS_ERROR:
                $response = $this->fromArray($responseData, Api\Server\Response\Error::class);
                break;
            default:
                throw new Api\Exception\DataMapperException(sprintf('Unknown response status: "%s"', $responseData['status']), Api\Exception\DataMapperException::CODE_INVALID_RESPONSE_STATUS);
        }

        if ($response === null) {
            return $response;
        }

        if ($response instanceof Api\Server\Response\Success) {
            $clearing = $this->fromArray($responseData, Api\Server\Response\Clearing::class);
            if ($clearing !== null) {
                $response->setClearing($clearing);
            }
        }

        return $response;
    }

    /**
     * @param array $callbackData
     * @return Notification\Request\TransactionStatus|null
     * @throws Exception\DataMapperException
     */
    public function mapTransactionStatusData(array $callbackData)
    {
        /** @var Api\Notification\Request\TransactionStatus|null $transactionStatus */
        $transactionStatus = $this->fromArray($callbackData, Api\Notification\Request\TransactionStatus::class);

        if ($transactionStatus === null) {
            return $transactionStatus;
        }

        if (($transactionStatus->getKey() != hash('md5', $this->getOption('key'))) ||
            ($transactionStatus->getPortalId() != $this->getOption('portalId')) ||
            ($transactionStatus->getSubAccountId() != $this->getOption('subAccountId'))) {
            throw new Api\Exception\DataMapperException(
                sprintf ('Invalid transaction status request credentials (key: "%s", portal id: "%s", subaccount id: "%s")', $transactionStatus->getKey(), $transactionStatus->getPortalId(), $transactionStatus->getSubAccountId()),
                Api\Exception\DataMapperException::CODE_INVALID_NOTIFICATION_CREDENTIALS);
        }

        $personalData = $this->fromArray($callbackData, Api\Notification\Request\PersonalData::class);
        if ($personalData !== null) {
            $transactionStatus->setPersonalData($personalData);
        }

        $deliveryData = $this->fromArray($callbackData, Api\Notification\Request\DeliveryData::class);
        if ($deliveryData !== null) {
            $transactionStatus->setDeliveryData($deliveryData);
        }

        return $transactionStatus;
    }

    private function fromArray(array $data, string $type)
    {
        $object = $this->arrayTransformer->fromArray($data, $type);

        if (empty($this->arrayTransformer->toArray($object, $this->createContext()))) {
            return null;
        }

        return $object;
    }

    /**
     * @return SerializationContext
     */
    private function createContext()
    {
        $groups = ['Default'];
        if ($this->getOption('business')) {
            $groups[] = 'business';
        }

        $context = SerializationContext::create();
        $context->setGroups($groups);

        return $context;
    }

    /**
     * @param array $data
     * @param string $key
     * @param $value
     * @throws Api\Exception\DataMapperException
     */
    private function addKeyValueToData(array &$data, string $key, $value): void
    {
        if (array_key_exists($key, $data)) {
            throw new Api\Exception\DataMapperException(sprintf('An entry with key "%s" allready exists', $key), Api\Exception\DataMapperException::CODE_KEY_ALLREADY_EXISTS);
        }

        $data[$key] = $value;
    }
}