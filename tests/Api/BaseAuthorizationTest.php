<?php

namespace Payone\Tests\Api;

use Payone\Api\Server\Request\Authorization;
use Payone\Api\Server\Request\BaseAuthorization;
use Payone\Api\Server\Request\ClearingType;
use Payone\Api\Server\Request\Preauthorization;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseAuthorizationTest extends TestCase
{
    /**
     * @var BaseAuthorization
     */
    private $request;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    protected function setUp(): void
    {
        parent::setUp();


        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();

    }

    public function clearingTypeValidationProvider()
    {
        return [
            [
                new Authorization(),
                null,
                1
            ], [
                new Authorization(),
                new ClearingType\Prepayment(),
                1
            ], [
                new Authorization(),
                new ClearingType\CreditCard(),
                0
            ], [
                new Authorization(),
                new ClearingType\CreditCardWithPseudoCardPan(),
                0
            ], [
                new Authorization(),
                new ClearingType\EWallet\PayPalExpress(),
                0
            ], [
                new Authorization(),
                new ClearingType\OnlineBankTransfer\SofortBanking(),
                0
            ],[
                new Preauthorization(),
                null,
                1
            ], [
                new Preauthorization(),
                new ClearingType\Prepayment(),
                0
            ], [
                new Preauthorization(),
                new ClearingType\CreditCard(),
                0
            ], [
                new Preauthorization(),
                new ClearingType\CreditCardWithPseudoCardPan(),
                0
            ], [
                new Preauthorization(),
                new ClearingType\EWallet\PayPalExpress(),
                0
            ], [
                new Preauthorization(),
                new ClearingType\OnlineBankTransfer\SofortBanking(),
                0
            ]
        ];
    }

    /**
     * @dataProvider clearingTypeValidationProvider
     * @param ClearingType|null $clearingType
     * @param $expectedViolationsCount
     */
    public function testClearingTypeValidation(BaseAuthorization $request, ?ClearingType $clearingType, $expectedViolationsCount)
    {

        if ($clearingType !== null) {
            $request->setClearingType($clearingType);
        }
        $clearingTypeViolationList = $this->findViolationsByPropertyPath(
            $this->validator->validate($request),
            'clearingType'
        );
        $this->assertEquals($expectedViolationsCount, $clearingTypeViolationList->count());
    }

    /**
     * @param ConstraintViolationListInterface|ConstraintViolationInterface[] $violationList
     * @param string $propertyPath
     * @return ConstraintViolationList
     */
    private function findViolationsByPropertyPath(ConstraintViolationListInterface $violationList, string $propertyPath)
    {

        $result = New ConstraintViolationList();

        foreach ($violationList as $violation) {
            if ($violation->getPropertyPath() === $propertyPath) {
                $result->add($violation);
            }
        }

        return $result;
    }
}