<?php

namespace Payone\Tests\Api;

use Payone\Api\DataMapper;
use Payone\Api\Exception\DataMapperException;
use Payone\Api\Server\Request\Authorization;
use Payone\Api\Server\Response;
use JMS\Serializer\ArrayTransformerInterface;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

class DataMapperTest extends TestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|ArrayTransformerInterface
     */
    private $arrayTransformerMock;

    /**
     * @throws \ReflectionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->arrayTransformerMock = $this->buildArrayTransformerMock();
    }

    /**
     * @return array
     */
    public function mapRequestEmbeddingDataProvider(): array
    {
        return [
            [
                [
                    'items' => [[
                        'key1' => 'value1',
                        'key2' => 'value2',
                    ], [
                        'key1' => 'value3',
                        'key2' => 'value4'
                    ]]
                ],
                'items',
                [
                    'key1[1]' => 'value1',
                    'key2[1]' => 'value2',
                    'key1[2]' => 'value3',
                    'key2[2]' => 'value4',
                ]
            ], [
                [
                    'clearing_type' => [
                        'key1' => 'value1',
                        'key2' => 'value2'
                    ]
                ],
                'clearing_type',
                [
                    'key1' => 'value1',
                    'key2' => 'value2'
                ]
            ], [
                [
                    'personal_data' => [
                        'key1' => 'value1',
                        'key2' => 'value2'
                    ]
                ],
                'personal_data',
                [
                    'key1' => 'value1',
                    'key2' => 'value2'
                ]
            ], [
                [
                    'delivery_data' => [
                        'key1' => 'value1',
                        'key2' => 'value2'
                    ]
                ],
                'delivery_data',
                [
                    'key1' => 'value1',
                    'key2' => 'value2'
                ]

            ]
        ];
    }

    /**
     * @dataProvider mapRequestEmbeddingDataProvider
     *
     * @throws \Payone\Api\Exception\DataMapperException
     */
    public function testMapRequestItemsEmbedding($data, $notExistingKey, $existingKeyValues)
    {

        $this->arrayTransformerMock
            ->expects($this->once())
            ->method('toArray')
            ->willReturn($data);

        $dataMapper = new DataMapper(
            [
                'merchantId' => 'theMerchantId',
                'portalId' => 'thePortalId',
                'key' => 'theKey',
                'subAccountId' => 'theSubAccountId'
            ],
            $this->arrayTransformerMock
        );

        $data = $dataMapper->mapRequest(new Authorization());

        $this->assertArrayNotHasKey($notExistingKey, $data);
        foreach ($existingKeyValues as $key => $value) {
            $this->assertArrayHasKey($key, $data);
            $this->assertEquals($data[$key], $value);
        }
    }

    /**
     * @throws \Payone\Api\Exception\DataMapperException
     */
    public function testMapRequestDataMapperExceptions()
    {

        $this->expectException(DataMapperException::class);
        $this->expectExceptionCode(DataMapperException::CODE_KEY_ALLREADY_EXISTS);

        $this->arrayTransformerMock
            ->expects($this->once())
            ->method('toArray')
            ->willReturn([
                'personal_data' => [
                    'key' => 'value'
                ],
                'delivery_data' => [
                    'key' => 'value'
                ]
            ]);

        $dataMapper = new DataMapper(
            [
                'merchantId' => 'theMerchantId',
                'portalId' => 'thePortalId',
                'key' => 'theKey',
                'subAccountId' => 'theSubAccountId'
            ],
            $this->arrayTransformerMock
        );

        $data = $dataMapper->mapRequest(new Authorization());
    }

    public function mapResponseDataProvider(): array
    {
        return [
            [
                [
                    'status' => Response::STATUS_APPROVED,
                    'txid' => '123',
                ],
                Response\Success\Approved::class
            ], [
                [
                    'status' => Response::STATUS_REDIRECT,
                    'txid' => '123',
                ],
                Response\Success\Redirect::class
            ], [
                [
                    'status' => Response::STATUS_PENDING,
                    'txid' => '123',
                ],
                Response\Success\Pending::class
            ], [
                [
                    'status' => Response::STATUS_ERROR,
                    'errorcode' => '123',
                ],
                Response\Error::class
            ]
        ];
    }

    /**
     * @dataProvider mapResponseDataProvider
     *
     * @param $responseData
     * @param $expectedResponseType
     * @throws DataMapperException
     */
    public function testMapResponse($responseData, $expectedResponseType)
    {
        /** @var Serializer $serializer */
        $serializer = SerializerBuilder::create()->build();
        $dataMapper = new DataMapper(
            [
                'merchantId' => 'theMerchantId',
                'portalId' => 'thePortalId',
                'key' => 'theKey',
                'subAccountId' => 'theSubAccountId'
            ],
            $serializer
        );

        $response = $dataMapper->mapResponse($responseData);

        $this->assertInstanceOf($expectedResponseType, $response);
    }

    public function mapResponseDataMapperExceptionsDataProvider(): array
    {
        return [
            [
                [],
                DataMapperException::class,
                DataMapperException::CODE_RESPONSE_DATA_MUST_NOT_BE_EMPTY
            ], [
                ['status' => 'INVALID STATE'],
                DataMapperException::class,
                DataMapperException::CODE_INVALID_RESPONSE_STATUS
            ]
        ];
    }

    /**
     * @dataProvider mapResponseDataMapperExceptionsDataProvider
     *
     * @throws DataMapperException
     */
    public function testMapResponseDataMapperExceptions(array $responseData, string $expectedException, int $expectedExceptionCode)
    {
        $this->expectException($expectedException);
        $this->expectExceptionCode($expectedExceptionCode);

        $dataMapper = new DataMapper(
            [
                'merchantId' => 'theMerchantId',
                'portalId' => 'thePortalId',
                'key' => 'theKey',
                'subAccountId' => 'theSubAccountId'
            ],
            $this->arrayTransformerMock
        );

        $dataMapper->mapResponse($responseData);
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject|ArrayTransformerInterface
     * @throws \ReflectionException
     */
    private function buildArrayTransformerMock()
    {
        return $this->getMockBuilder(ArrayTransformerInterface::class)
            ->setMethods(['toArray', 'fromArray'])
            ->getMock();
    }
}