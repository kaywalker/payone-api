<?php

namespace Payone\Tests\Api;

use GuzzleHttp\Psr7\Response;
use Payone\Api\Server;
use Payone\Api\DataMapper;

use Payone\Api\Event\PaymentProcessUpdateEvent;
use Payone\Api\Exception\InvalidApiResponse;
use Payone\Api\Server\Request\Authorization;
use Payone\Api\Server\Response\Success\Approved;
use Payone\Api\ResponseParser;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class ClientTest extends TestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|DataMapper
     */
    private $dataMapperMock;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|\GuzzleHttp\Client
     */
    private $httpClientMock;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|ResponseParser
     */
    private $responseParserMock;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|EventDispatcher
     */
    private $dispatcherMock;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|Server
     */
    private $client;

    /**
     * @throws \ReflectionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->dataMapperMock = $this->buildDataMapperMock();
        $this->httpClientMock = $this->buildHttpClientMock();
        $this->responseParserMock = $this->buildResponseParserMock();
        $this->dispatcherMock = $this->buildDispatcherMock();
        $this->client = new Server(
            $this->dataMapperMock,
            $this->httpClientMock,
            $this->responseParserMock,
            $this->dispatcherMock,
            $this->buildLoggerMock()
        );
    }

    /**
     * @throws InvalidApiResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Payone\Api\Exception\DataMapperException
     */
    public function testRequest()
    {
        $request = new Authorization();
        $response = new Approved();
        $requestEvent = new PaymentProcessUpdateEvent\Request($request, $response);

        $this->dataMapperMock
            ->expects($this->once())
            ->method('mapRequest')
            ->willReturn([]);

        $this->dataMapperMock
            ->expects($this->once())
            ->method('mapResponse')
            ->willReturn($response);

        $this->responseParserMock
            ->expects($this->once())
            ->method('parse');

        $this->httpClientMock
            ->expects($this->once())
            ->method('request')
            ->willReturn(new Response(200, [], 'content'));

        $this->dispatcherMock
            ->expects($this->once())
            ->method('dispatch')
            ->with($requestEvent);

        $this->client->request($request);
    }

    /**
     * @dataProvider requestDataProvider
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Payone\Api\Exception\DataMapperException
     * @throws \Payone\Api\Exception\InvalidApiResponse
     */
    public function testRequestExeptions(Response $response, string $expectedException, int $expectedExceptionCode)
    {
        $this->expectException($expectedException);
        $this->expectExceptionCode($expectedExceptionCode);

        $this->dataMapperMock
            ->expects($this->once())
            ->method('mapRequest')
            ->willReturn([]);

        $this->httpClientMock
            ->expects($this->once())
            ->method('request')
            ->willReturn($response);

        $this->client->request(new Authorization());
    }

    /**
     * @return array
     */
    public function requestDataProvider()
    {
        return [[
            new Response(400),
            InvalidApiResponse::class,
            InvalidApiResponse::CODE_INVALID_STATUS_CODE
        ], [
            new Response(),
            InvalidApiResponse::class,
            InvalidApiResponse::CODE_EMPTY_BODY
        ]];
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject|LoggerInterface
     * @throws \ReflectionException
     */
    private function buildLoggerMock()
    {
        return $this->getMockBuilder(LoggerInterface::class)
            ->setMethods(['info'])
            ->getMock();
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject|\GuzzleHttp\Client
     * @throws \ReflectionException
     */
    private function buildHttpClientMock()
    {
        return $this->getMockBuilder(\GuzzleHttp\Client::class)
            ->getMock();
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject|DataMapper
     * @throws \ReflectionException
     */
    private function buildDataMapperMock()
    {
        return $this->getMockBuilder(DataMapper::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject|ResponseParser
     * @throws \ReflectionException
     */
    private function buildResponseParserMock()
    {
        return $this->getMockBuilder(ResponseParser::class)
            ->getMock();
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject|EventDispatcher
     * @throws \ReflectionException
     */
    private function buildDispatcherMock()
    {
        return $this->getMockBuilder(EventDispatcher::class)
            ->getMock();
    }
}