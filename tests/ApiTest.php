<?php

namespace Payone\Test;

use Payone\Api;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ApiTest extends TestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testConstructor()
    {
        /** @var MockObject|EventDispatcherInterface $dispatcher */
        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)
            ->getMock();

        /** @var MockObject|LoggerInterface $logger */
        $logger = $this->getMockBuilder(LoggerInterface::class)
            ->getMock();

        $api = new Api(
            [
                'merchantId' => 'theMerchantId',
                'portalId' => 'thePortalId',
                'key' => 'theKey',
                'subAccountId' => 'theSubAccountId'
            ],
            $dispatcher,
            $logger
        );

        $this->assertInstanceOf(Api\Server::class, $api->getServer());
        $this->assertInstanceOf(Api\Notification::class, $api->getNotification());
    }
}